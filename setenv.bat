@echo off

:: Run with active environment
call %~dp0active.bat

:: Development environment root
set WNIX_HOME=%~dp0

:: Java Development Kit
set JAVA_HOME=%WNIX_HOME%jdk

:: Minimalist GNU for Windows (compiler, linker, etc...)
set MINGW_HOME=%WNIX_HOME%mingw\qt5\Tools\mingw530_32

:: Qt5 Libraries
if %ACTIVE_ENV% == msvc (
    set QT5LIBS_HOME=%WNIX_HOME%%ACTIVE_ENV%\qt5\5.8\msvc2013
) else (
    set QT5LIBS_HOME=%WNIX_HOME%%ACTIVE_ENV%\qt5\5.8\mingw53_32
)

:: QtCreator
set QTCREATOR_HOME=%WNIX_HOME%%ACTIVE_ENV%\qt5\Tools\QtCreator\bin

:: QtDesigner
set QTDESIGNER_HOME=%QT5LIBS_HOME%\bin

:: FreeGLUT
set FREEGLUT_HOME=%WNIX_HOME%%ACTIVE_ENV%\freeglut

:: Glew
set GLEW_HOME=%WNIX_HOME%%ACTIVE_ENV%\glew

:: Python
set PYTHON_HOME=%WNIX_HOME%python3

:: Cmder Terminal Emulator
set CMDER_HOME=%WNIX_HOME%cmder

:: Perl
set PERL_HOME=%WNIX_HOME%perl

:: Svn
set SVN_HOME=%WNIX_HOME%svn

:: CMake
set CMAKE_HOME=%WNIX_HOME%cmake
set CMAKE_PREFIX_PATH=%QT5LIBS_HOME%;%FREEGLUT_HOME%;%GLEW_HOME%

:: Eclipse
set ECLIPSE_HOME=%WNIX_HOME%eclipse

:: VSCode
set VSCODE_HOME=%WNIX_HOME%vscode

::Ninja
set NINJA_HOME=%WNIX_HOME%ninja

:: Prepend PATH
set PATH=%WNIX_HOME%\doxygen;%PATH%
set PATH=%NINJA_HOME%;%PATH%
set PATH=%VSCODE_HOME%;%PATH%
set PATH=%ECLIPSE_HOME%;%PATH%
set PATH=%SVN_HOME%\bin;%PATH%
set PATH=%CMAKE_HOME%\bin;%PATH%
set PATH=%PYTHON_HOME%\Scripts;%PATH%
set PATH=%PYTHON_HOME%;%PATH%
set PATH=%MINGW_HOME%\bin;%PATH%
set PATH=%JAVA_HOME%\bin;%PATH%
set PATH=%FREEGLUT_HOME%\bin;%PATH%
set PATH=%GLEW_HOME%\bin;%PATH%
set PATH=%QT5LIBS_HOME%\bin;%PATH%
set PATH=%CMDER_HOME%\vendor\git-for-windows\cmd;%PATH%
set PATH=%WNIX_HOME%imagemagick\bin;%PATH%
